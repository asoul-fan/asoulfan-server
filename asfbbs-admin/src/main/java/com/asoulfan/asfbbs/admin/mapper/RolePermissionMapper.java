package com.asoulfan.asfbbs.admin.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.asoulfan.asfbbs.admin.domain.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface RolePermissionMapper extends BaseMapper<RolePermission> {
    
    
}
